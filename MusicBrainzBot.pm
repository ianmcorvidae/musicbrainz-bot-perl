#!/usr/bin/perl

package MusicBrainzBot;
use FindBin;
use lib "$FindBin::Bin";
use utf8;
use parent 'MusicBrainzBot::Bot';

use MusicBrainzBot::Settings;
use MusicBrainzBot::DB;
#use MusicBrainzBot::Website;

binmode STDOUT, ":utf8";

sub new {
	my ($package, $args) = @_;

	my $settings = MusicBrainzBot::Settings->new($args);
	my $dbh = MusicBrainzBot::DB->new($settings);
	my $self = $package->SUPER::new($dbh);
	return $self;

#	my $bot = MusicBrainzBot::Website->new($dbh);
#	my $args = "";

	my $self = {
		'dbh' => $dbh,
		'settings' => $settings,
#		'bot' => $bot,
#		'args' => $args,
#		'server' => $settings->get("server"),
#		'protocol' => $settings->get("protocol"),
#		'username' => $settings->get("username"),
#		'password' => $settings->get("password"),
#		'useragent' => 'MusicBrainz bot/0.1',
#		'verbose' => $args->{verbose},
	};
	bless $self, $package;
}

sub print {
	my ($self, $text) = @_;

	print $text if $self->{settings}->{verbose};
}

1;

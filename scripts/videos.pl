#!/usr/bin/perl
# perl bot.pl [options]

use FindBin;
use lib "$FindBin::Bin/..";
use strict;
use utf8;

use MusicBrainzBot;
use Storable;
use JSON;
use LWP::Simple;
use List::MoreUtils qw(uniq);

my $bot = MusicBrainzBot->new;
my $max = $bot->{settings}->{max};

my $previous = {};
$previous = retrieve('videos.cache') if -e 'videos.cache';

# Fetch some recordings MBIDs from the webservice

our @mbids = ();
get_mbids(0);

sub get_mbids {
	my $offset = shift;

	my $url = "https://beta.musicbrainz.org/ws/2/recording?query=format%3Advd-video+AND+NOT+video%3Atrue+AND+NOT+format%3Acd+AND+NOT+country%3A%28us%29&limit=100&fmt=json&offset=$offset";
	$bot->print("Getting $url\n");
	my $json = get($url);
	sleep(1);
	my $data = from_json($json);

	for my $rec (@{ $data->{recording} }) {
#		next if scalar @{ $rec->{releases} } > 1;
		push @mbids, $rec->{id};
	}

	get_mbids($offset+100) if $offset < $data->{count} and scalar @mbids < 5000;
}

die "No MBIDs found.\n" unless scalar @mbids > 0;

# Load them from the database

my $dbh = $bot->{dbh};
my $sth = $dbh->prepare("
	select d.gid, d.name, d.artist_credit, array_agg(mf.name order by mf.name)
	from recording d
	join track t on t.recording = d.id
	join medium m on m.id = t.medium
	left join medium_format mf on mf.id = m.format
	where d.gid in (".placeholders(@mbids).")
	and d.video = 'f'
	and d.edits_pending = 0
	group by d.gid, d.name, d.artist_credit
	order by d.artist_credit
");
$sth->execute(@mbids);

while (my ($mbid, $name, $ac, $formats) = $sth->fetchrow()) {
	unless ($max > 0) {
		$bot->print("Reached maximum number of files.\n");
		last;
	}

	next if $previous->{$mbid};
	next unless join(";", uniq @$formats) eq "DVD-Video";

	my $formatlist = join ";", uniq @$formats;
	my $releasecount = scalar @$formats;

	my $edit_note = "";
	if ($releasecount > 1) {
		$edit_note = "This recording is used on $releasecount releases and the mediums it appears on are all set to DVD-Video";
	} else {
		$edit_note = "This recording is only used once on a release and the medium it appears on is set to DVD-Video";
	}

	$bot->print("Setting https://beta.musicbrainz.org/recording/$mbid to video (formats list: ".join("; ", @$formats).")\nRecording name: $name\n\n");
	my $rv = $bot->edit_recording($mbid, { 'video' => 1, 'edit_note' => $edit_note, 'as_auto_editor' => 1 });
	$previous->{$mbid}++ if $rv > 0;
	$max -= $rv;
}

# TODO: This file is going to get huge. Should generate a second hash of edited things which were still found and save that instead.
store $previous, 'videos.cache' unless $bot->{settings}->{dryrun};

#$dbh->disconnect(); # TODO: This fails when quitting if it's not finished fetching all the rows

sub placeholders {
	return join ",", ("?") x scalar(@_);
}


#!/usr/bin/perl
# perl bot.pl [options]

use FindBin;
use lib "$FindBin::Bin/..";
use utf8;

use MusicBrainzBot;
use Unicode::Normalize;

my $bot = MusicBrainzBot->new;
my $max = $bot->{settings}->{max};

my $sth = $bot->{dbh}->prepare("
	SELECT e.gid, e.name, e.comment
	FROM label e
	where (
		e.name !~ E'^[ -~]+\$'
		OR e.comment !~ E'^[ -~]+\$'
	)
	AND e.edits_pending = 0
	ORDER BY e.name ASC
");
$sth->execute;

while (my ($mbid, $name, $comment) = $sth->fetchrow()) {
	unless ($max > 0) {
		$bot->print("Reached maximum number of files.\n");
		last;
	}

	my $data = { 'edit_note' => 'Fixing various character issues (removing control characters and unnecessary whitespace, replacing unnecessary combining characters...)', 'as_auto_editor' => 1 };
	my @notes = ();
	my $name_orig = $name;
	my $comment_orig = $comment;

	$name = clean($name);
	$comment = clean($comment);

	next if $name_orig eq $name && $comment_orig eq $comment;
	$data->{'name'} = $name if $name_orig ne $name;
	$data->{'comment'} = $comment if $comment_orig ne $comment;

	$bot->print("Editing $mbid:\nOld: $name_orig, $comment_orig\nNew: $name, $comment\n");
	my $rv = $bot->edit_label($mbid, $data);
	$max -= $rv;
}

sub clean {
	my $str = shift;

	print "contains tabs: $str\n" if $str =~ /\t/;
	$str =~ s/\t/ /g;

	print "contains zwsps: $str\n" if $str =~ /\x{200B}/;
	$str =~ s/\x{200B}//g; # zwsp

	print "contains shys: $str\n" if $str =~ /\x{00AD}/;
	$str =~ s/\x{00AD}//g; # shy

	print "contains lrm: $str\n" if $str =~ /\x{200E}/ && $str !~ /[\p{Arabic}\p{Hebrew}]/;
	print "contains rlm: $str\n" if $str =~ /\x{200F}/ && $str !~ /[\p{Arabic}\p{Hebrew}]/;
	$str =~ s/\x{200E}//g if $str !~ /[\p{Arabic}\p{Hebrew}]/; #unless /\p{R}/; # lrm - unless text contains RTL characters
	$str =~ s/\x{200F}//g if $str !~ /[\p{Arabic}\p{Hebrew}]/; # unless /\p{R}/; # rlm - un less text contains RTL characters

	print "contains control chars: $str\n" if $str =~ /\p{Cc}/;
	$str =~ s/\p{Cc}//g; # control characters

	print "not nfc: $str\n" if $str ne NFC($str) && $str =~ /[\p{Latin}\p{Greek}\p{Cyrillic}\p{Hiragana}\p{Katakana}]\p{M}/;
	$str = NFC($str) if $str ne NFC($str) && $str =~ /[\p{Latin}\p{Greek}\p{Cyrillic}\p{Hiragana}\p{Katakana}]\p{M}/;

	$str =~ s/  +/ /g;
	$str =~ s/^ +//;
	$str =~ s/ +$//;

	return $str;
}

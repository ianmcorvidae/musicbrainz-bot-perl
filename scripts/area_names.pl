#!/usr/bin/perl
# perl bot.pl [options]

use FindBin;
use lib "$FindBin::Bin/..";
use utf8;

use MusicBrainzBot;

my $bot = MusicBrainzBot->new({ settings => "area_bot.json" });
my $max = $bot->{settings}->{max};

$bot->login();

my $sth = $bot->{dbh}->prepare("
	select a.gid, a.name, a0.name, a1.name from area a join l_area_area laa on laa.entity1=a.id join area a0 on a0.id=laa.entity0 left join l_area_area laa1 on laa.entity0=laa1.entity1 left join area a1 on a1.id=laa1.entity0 where a.name ~ ', ' order by a.name
");
$sth->execute;

while (my ($mbid, $name, $parent, $parent2) = $sth->fetchrow()) {
	unless ($max > 0) {
		$bot->print("Reached maximum number of files.\n");
		last;
	}

	my $origname = $name;
	$name =~ s/(.*), (.*)/$1/;
	my $suffix = $2;
	my $editnote = 'Removing parent area name from area name';

	$suffix = "Valle d'Aosta" if $suffix eq "Aosta Valley";

	$suffix = "Niedersachsen" if $suffix eq "Lower Saxony";
	$suffix = "Rheinland-Pfalz" if $suffix eq "Rhineland-Palatinate";
	$suffix = "Trento" if $suffix eq "Trentino";
	$suffix = "Thüringen" if $suffix eq "Thuringia";

	$suffix = "Toscana" if $suffix eq "Tuscany";
	$suffix = "Sicilia" if $suffix eq "Sicily";
	$suffix = "Sardegna" if $suffix eq "Sardinia";
	$suffix = "Piemonte" if $suffix eq "Piedmont";
	$suffix = "Lombardia" if $suffix eq "Lombardy";
	$suffix = "Bayern" if $suffix eq "Bavaria";
	$suffix = "Puglia" if $suffix eq "Apulia";
	$suffix = "Sachsen" if $suffix eq "Saxony";

	$suffix = "Harjumaa" if $suffix eq "Harju County";
	$suffix = "Ida-Virumaa" if $suffix eq "Ida-Viru County";
	$suffix = "Järvamaa" if $suffix eq "Järva County";
	$suffix = "Läänemaa" if $suffix eq "Lääne County";
	$suffix = "Lääne-Virumaa" if $suffix eq "Lääne-Viru County";
	$suffix = "Pärnumaa" if $suffix eq "Pärnu County";
	$suffix = "Raplamaa" if $suffix eq "Rapla County";
	$suffix = "Saaremaa" if $suffix eq "Saare County";
	$suffix = "Viljandimaa" if $suffix eq "Viljandi County";
	$suffix = "Valgamaa" if $suffix eq "Valga County";
	$suffix = "Võrumaa" if $suffix eq "Võru County";

#	$suffix = "" if $suffix eq "";

	if ($parent2 eq "United States" && $suffix =~ /^(St\. )?[A-Za-z -]+ County$/) {
		$bot->print("Editing $mbid:\nOld: $origname\nNew: $name\n\n");
		my $rv = $bot->edit_area($mbid, { 'edit_note' => "Moving disambiguating text to disambiguation", 'name' => $name, 'sort_name' => $name, 'comment' => $suffix, 'as_auto_editor' => 0 });
		$max -= $rv;
	}

	next if $origname eq $name;
	next unless $suffix eq $parent or $suffix eq $parent2;

	$bot->print("Editing $mbid:\nOld: $origname\nNew: $name\n\n");
	my $rv = $bot->edit_area($mbid, { 'edit_note' => 'Removing parent area name from area name', 'name' => $name, 'sort_name' => $name, 'as_auto_editor' => 1 });
	$max -= $rv;
}


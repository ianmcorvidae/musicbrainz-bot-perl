#!/usr/bin/perl
# perl bot.pl [options]

use FindBin;
use lib "$FindBin::Bin/..";
use utf8;

use MusicBrainzBot;

my $bot = MusicBrainzBot->new;
my $max = $bot->{settings}->{max};
$bot->login();

my $sth = $bot->{dbh}->prepare("
	select gid, url
	from url
	where url ~ '^http://www.discogs.com/master/view/'
	and edits_pending = 0
	order by url
");
$sth->execute;

while (my ($gid, $url) = $sth->fetchrow()) {
	unless ($max > 0) {
		$bot->print("Reached maximum number of files.\n");
		last;
	}
	
	my $newurl = $url;
	$newurl =~ s/\/master\/view/\/master/;

	$bot->print("Editing Discogs URL:\nOld: $url\nNew: $newurl\n\n");
	my $rv = $bot->edit_url($gid, { url => $newurl, as_auto_editor => 1, edit_note => "Standardising Discogs URLs" });
	$max -= $rv;
}


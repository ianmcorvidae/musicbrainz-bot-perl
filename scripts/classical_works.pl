#!/usr/bin/perl
# perl bot.pl [options]

use FindBin;
use lib "$FindBin::Bin/..";
use utf8;

use MusicBrainzBot;

my $bot = MusicBrainzBot->new;
my $max = $bot->{settings}->{max};

$bot->login();

my $sth = $bot->{dbh}->prepare("
	select w.gid, w.name
	from work w
	where w.edits_pending = 0
	and w.name ~* 'in [a-g]([ -](sharp|flat))? (major|minor)'
	and w.name ~ E' Op\. ?[0-9]'
");
$sth->execute;

while (my ($mbid, $name) = $sth->fetchrow()) {
	unless ($max > 0) {
		$bot->print("Reached maximum number of files.\n");
		last;
	}

	my $newname = $name;
	$newname =~ s/(?<!:) No\. ?([0-9])/ no. $1/g;
	$newname =~ s/(?<!:) Op\. ?([0-9])/ op. $1/g;

	$newname =~ s/ in ([A-G])[ -][Ss]harp / in $1-sharp /g;
	$newname =~ s/ in ([A-G])[ -][Ff]lat / in $1-flat /g;
	$newname =~ s/ in ([A-G](-(sharp|flat))?) [Mm]ajor/ in $1 major/g;
	$newname =~ s/ in ([A-G](-(sharp|flat))?) [Mm]inor/ in $1 minor/g;

	next if $name eq $newname;

	$bot->print("Editing work $name ($mbid):\nOld: $name\nNew: $newname\n\n");
	my $rv = $bot->edit_work($mbid, { 'edit_note' => 'https://musicbrainz.org/doc/Style/Classical/Language/English', 'name' => $newname });
	$max -= $rv;
}


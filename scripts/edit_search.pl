#!/usr/bin/perl
# perl bot.pl [options]

use FindBin;
use lib "$FindBin::Bin/..";
use utf8;

use MusicBrainzBot;

my $bot = MusicBrainzBot->new({ settings => "nikki.json" });
$bot->login();

my %searches = (
#	"Set track lengths" => "conditions.0.field=type&conditions.0.operator=%3D&conditions.0.args=58%2C253&conditions.1.field=status&conditions.1.operator=%3D&conditions.1.args=1&conditions.2.field=vote&conditions.2.operator=%3D&conditions.2.voter_id=53705&conditions.2.args=no",
	"Karaoke versions" => "conditions.3.field=link_type&conditions.3.operator=%3D&conditions.3.args=226&conditions.1.field=status&conditions.1.operator=%3D&conditions.1.args=1&conditions.2.field=vote&conditions.2.operator=%3D&conditions.2.voter_id=53705&conditions.2.args=no&conditions.4.field=editor&conditions.4.operator=%21%3D&conditions.4.name=nikki&conditions.4.args.0=53705",
#	"Place edits" => "conditions.0.field=type&conditions.0.operator=%3D&conditions.0.args=62&conditions.0.args=63&conditions.0.args=61&conditions.0.args=64&conditions.0.args=68&conditions.0.args=67&conditions.0.args=66&conditions.0.args=65&conditions.1.field=status&conditions.1.operator=%3D&conditions.1.args=1&conditions.2.field=vote&conditions.2.operator=%3D&conditions.2.voter_id=53705&conditions.2.args=no",
	"Cover art relationships" => "conditions.1.field=status&conditions.1.operator=%3D&conditions.1.args=1&conditions.2.field=vote&conditions.2.operator=%3D&conditions.2.voter_id=53705&conditions.2.args=no&conditions.3.field=link_type&conditions.3.operator=%3D&conditions.3.args=78&conditions.4.field=editor&conditions.4.operator=%21%3D&conditions.4.name=nikki&conditions.4.args.0=53705",
	"Part of set relationships" => "conditions.1.field=status&conditions.1.operator=%3D&conditions.1.args=1&conditions.2.field=vote&conditions.2.operator=%3D&conditions.2.voter_id=53705&conditions.2.args=no&conditions.3.field=link_type&conditions.3.operator=%3D&conditions.3.args=1&conditions.4.field=editor&conditions.4.operator=%21%3D&conditions.4.name=nikki&conditions.4.args.0=53705",
);

for my $search (sort keys %searches) {
	my $results = $bot->edit_search($searches{$search});
	if ($results > 0) {
		print "$results edits for $search\nhttps://beta.musicbrainz.org/search/edits?$searches{$search}\n\n";
	}
}

my %types = (
#	"71" => "Alleinstehende Aufnahme hinzufügen",
##	"72" => "Aufnahme bearbeiten",
#	"73" => "Aufnahme entfernen",
##	"74" => "Aufnahmen zusammenführen",
##	"91" => "Beziehung bearbeiten",
#	"92" => "Beziehung entfernen",
##	"90,233" => "Beziehung hinzufügen",
#	"316" => "Cover-Art bearbeiten",
#	"315" => "Cover-Art entfernen",
	"22" => "Set cover art",
##	"314" => "Cover-Art hinzufügen",
#	"220,54" => "Disc-ID entfernen",
#	"221,56" => "Disc-ID verschieben",
#	"78" => "ISRC entfernen",
#	"410" => "ISWC entfernen",
#	"2" => "Künstler bearbeiten",
#	"3" => "Künstler entfernen",
#	"4" => "Künstler zusammenführen",
#	"8" => "Künstleralias bearbeiten",
#	"7" => "Künstleralias entfernen",
#	"6" => "Künstleralias hinzufügen",
#	"9" => "Künstlernennung bearbeiten",
#	"11" => "Label bearbeiten",
#	"13" => "Label entfernen",
#	"14" => "Label zusammenführen",
#	"18" => "Labelalias bearbeiten",
#	"17,262" => "Labelalias entfernen",
#	"16" => "Labelalias hinzufügen",
#	"313" => "Medien umsortieren",
#	"52" => "Medium bearbeiten",
#	"53" => "Medium entfernen",
#	"51" => "Medium hinzufügen",
#	"39" => "Strichcodes bearbeiten",
	"58,253" => "Set track lengths",
	"101" => "Edit URL",
#	"32" => "Veröffentlichung bearbeiten",
#	"310,212" => "Veröffentlichung entfernen",
##	"31,216" => "Veröffentlichung hinzufügen",
#	"225,311,223" => "Veröffentlichungen vereinen",
#	"21" => "Veröffentlichungsgruppe bearbeiten",
#	"23" => "Veröffentlichungsgruppe entfernen",
#	"20" => "Veröffentlichungsgruppe hinzufügen",
#	"273" => "Veröffentlichungsgruppe ändern",
#	"24" => "Veröffentlichungsgruppen vereinen",
#	"312" => "Veröffentlichungskünstler bearbeiten",
#	"37" => "Veröffentlichungslabel bearbeiten",
#	"36" => "Veröffentlichungslabel entfernen",
##	"34" => "Veröffentlichungslabel hinzufügen",
#	"263,38" => "Veröffentlichungsqualität ändern",
#	"42" => "Werk bearbeiten",
#	"43" => "Werk entfernen",
#	"48" => "Werkalias bearbeiten",
#	"47" => "Werkalias entfernen",
#	"46" => "Werkalias hinzufügen",
#	"44" => "Werke zusammenführen",
#	"62" => "Örtlichkeit bearbeiten",
#	"63" => "Örtlichkeit entfernen",
#	"64" => "Örtlichkeiten vereinen",
#	"68" => "Örtlichkeitsalias bearbeiten",
#	"67" => "Örtlichkeitsalias entfernen",
#	"66" => "Örtlichkeitsalias hinzufügen",

	"75,234,235,97,94,98,95,93,96,317,82,83,81,84,88,87,86,85,76,49,1,5,252,10,15,113,77,211,218,207,210,245,204,205,213,209,208,224,231,35,226,229,250,251,249,33,25,201,244,41,45,120,61,65,55,232" => "auto-edits - should be empty",

);



for my $type (sort keys %types) {
	my $subsearch = join "&conditions.0.args=", (split /,/, $type);
	my $search = "conditions.0.field=type&conditions.0.operator=%3D&conditions.0.args=$type&conditions.1.field=status&conditions.1.operator=%3D&conditions.1.args=1&conditions.2.field=vote&conditions.2.operator=%3D&conditions.2.voter_id=53705&conditions.2.args=no&conditions.3.field=editor&conditions.3.operator=%21%3D&conditions.3.name=nikki&conditions.3.args.0=53705";
	my $results = $bot->edit_search($search);
	if ($results > 0) {
		print "$results edits for $types{$type}\nhttps://beta.musicbrainz.org/search/edits?$search\n\n";
	}
}


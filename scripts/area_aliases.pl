#!/usr/bin/perl
# perl bot.pl [options]

use FindBin;
use lib "$FindBin::Bin/..";
use utf8;
use strict;

use MusicBrainzBot;
use MusicBrainzArea;

binmode STDOUT, ":utf8";

my $bot = MusicBrainzBot->new({ settings => "area_bot.json" });
my $dbh = $bot->{dbh};

my $sth = $dbh->prepare("
WITH RECURSIVE area_descendants AS (
	SELECT entity0 AS parent, entity1 AS descendant, ARRAY[entity1] AS descendants
	FROM l_area_area laa
	JOIN link ON laa.link = link.id
	WHERE link_type = 356

	UNION ALL

	SELECT entity0 AS parent, descendant, descendants || entity1
	FROM l_area_area laa
	JOIN link ON laa.link=link.id
	JOIN area_descendants ON area_descendants.parent = laa.entity1
	WHERE link_type = 356
	AND NOT entity0 = ANY(descendants)),

parent_countries AS (
	SELECT DISTINCT ON (descendant) descendant, parent FROM area_descendants
	JOIN area ON area_descendants.parent = area.id
	WHERE area.type = 1 ORDER BY descendant, array_length(descendants, 1) ASC)

select a.gid, a.name
from area a
join parent_countries pc on pc.descendant = a.id
join area a2 on a2.id = pc.parent
join iso_3166_1 i on i.area = a2.id
where i.code not in ('FR', 'IT', 'US', 'CA', 'GB')
and a.name !~ '^[A-V]'
order by a.name");
$sth->execute;

while (my ($mbid, $name) = $sth->fetchrow()) {
	$bot->print("Creating area objecty thingy for $name...\n");
	my $area = MusicBrainzArea->new($dbh, $mbid);

	my @languages = $bot->{settings}->get_languages($area->country);
	for my $language (@languages) {
		next unless $language;

		my $alias = $area->compare($language);

		if ($alias && $alias ne "1" && ($alias ne $name || $language eq "en")) {
			$bot->print("Adding alias $alias as locale $language to $mbid ($name)...\n");
			$bot->add_alias($mbid, 'area', { name => $alias, sort_name => $alias, type_id => 1, locale => $language, primary_for_locale => 1 });
		}
	}

	$bot->print("\n");
#	$area->print_details();
}



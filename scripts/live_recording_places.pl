#!/usr/bin/perl
# perl bot.pl [options]

use FindBin;
use lib "$FindBin::Bin/..";
use utf8;

use MusicBrainzBot;

require "live_recording_places_mapping.pl";
my %mapping = mapping();

my $bot = MusicBrainzBot->new;
my $max = $bot->{settings}->{max};

$bot->login();

my $sth = $bot->{dbh}->prepare("
	select d.gid, d.comment
	from recording d
	left join l_place_recording lpd on lpd.entity1=d.id
	where d.comment != ''
	and d.comment ~ '^live, [0-9]{4}(-[0-9]{2}){0,2}: .*, (Germany|Japan|Australia|South Africa|Argentina|Thailand|Philippines|Israel|Spain|Norway|Turkey|Slovenia|Sweden|Finland|Belgium|Italy|Switzerland|Netherlands|Poland|Germany|Finland|Estonia)\$'
	and lpd.id is null
	and d.edits_pending = 0
	order by d.comment
");
$sth->execute;

while (my ($mbid, $name) = $sth->fetchrow()) {
	unless ($max > 0) {
		$bot->print("Reached maximum number of files.\n");
		last;
	}

	next unless $name =~ /^live, ([0-9]{4})(?:-([0-9]{2}))(?:-([0-9]{2})): (.*)$/;
	my ($y, $m, $d, $loc) = ($1, $2, $3, $4);
	next unless $mapping{$loc};

	$bot->print("Editing $name ($mbid)\n");

	my $rv = $bot->edit_relationship_new({
		"rel-editor.rels.0.action" => "add",
		"rel-editor.rels.0.link_type" => 693,
		"rel-editor.rels.0.entity.1.gid" => $mbid,
		"rel-editor.rels.0.entity.1.type" => "recording",
		"rel-editor.rels.0.entity.0.gid" => $mapping{$loc},
		"rel-editor.rels.0.entity.0.type" => "place",

		'rel-editor.rels.0.period.begin_date.year' => $y,
		'rel-editor.rels.0.period.begin_date.month' => $m,
		'rel-editor.rels.0.period.begin_date.day' => $d,
		'rel-editor.rels.0.period.end_date.year' => $y,
		'rel-editor.rels.0.period.end_date.month' => $m,
		'rel-editor.rels.0.period.end_date.day' => $d,
		'rel-editor.as_auto_editor' => 1,
		'rel-editor.edit_note' => 'from recording disambiguation'
	});

	$max -= $rv;
}


#!/usr/bin/perl
# perl bot.pl [options]

use FindBin;
use lib "$FindBin::Bin/..";
use utf8;
use open ':utf8';

use MusicBrainzBot;

use LWP::UserAgent;
my $ua = LWP::UserAgent->new;
$ua->agent("area_bot/0.1");

binmode STDOUT, ":utf8";

my $wikipedialt = 355;
my $wikidatalt = 358;
my $partoflt = 356;
my $datafile = "data.tsv";
my $parentsfile = "parents.tsv";

my $bot = MusicBrainzBot->new({ settings => "area_bot.json" });
my $max = $bot->{settings}->{max};
my $dryrun = $bot->{settings}->{dryrun};

$bot->login();

my @data = ();
open FILE, $datafile or die;
while (<FILE>) {
	chomp;
	my ($name, $type, $wd, $wp, $parent) = split /\t/;
	push @data, { name => $name, type => $type, wikidata => $wd, wikipedia => $wp, parent => $parent };
}
close FILE;

my $parents = {};
open FILE, $parentsfile or die;
while (<FILE>) {
	chomp;
	my ($mbid, $parent) = split /\t/;
	$parents->{$parent} = $mbid;
}
close FILE;

for my $item (@data) {
	unless ($max > 0) {
		$bot->print("Reached maximum number of files.\n");
		last;
	}

	if (!$item->{name}) {
		print "No name\n";
		next;
	}

	if ($item->{parent} !~ /^[0-9a-f-]{36}$/) {
		$item->{parent} = $parents->{ $item->{parent} };
	}

	if (!$item->{parent}) {
		print "No parent\n";
		next;
	}

	if (!$item->{wikidata}) {
		print "No Wikidata page\n";
		next;
	}

	if (in_mb($item->{wikidata})) {
		print "$item->{wikidata} already used in an URL in MusicBrainz.\n";
		next;
	}

	my $data = {
		name => $item->{name},
		sort_name => $item->{name},
		type_id => $item->{type},
		as_auto_editor => 1,
	};

	$data->{"url.0.text"} = $item->{wikidata} if $item->{wikidata};
	$data->{"url.0.link_type_id"} = $wikidatalt if $item->{wikidata};

	$data->{"url.1.text"} = $item->{wikipedia} if $item->{wikipedia};
	$data->{"url.1.link_type_id"} = $wikipedialt if $item->{wikipedia};

	$data->{"rel.0.link_type_id"} = $partoflt;
	$data->{"rel.0.target"} = $item->{parent};
	$data->{"rel.0.backward"} = 1;

	$bot->print("Adding area $item->{name}\n");
	my $mbid = $bot->add_area($data);

	$max--;
}

sub in_mb {
	my $wd = shift;

	my $url = "https://beta.musicbrainz.org/ws/2/url?resource=$wd&inc=area-rels&fmt=json";
	print "$url\n";
	my $r = $ua->get($url);
	sleep 1;
	if ($r->code eq "404") {
		return 0;
	} else {
		return 1;
	}
}

#!/usr/bin/perl
# perl bot.pl [options]

use FindBin;
use lib "$FindBin::Bin/..";
use strict;
use utf8;

use MusicBrainzBot;

my $bot = MusicBrainzBot->new;
my $max = $bot->{settings}->{max};
#$bot->login();

my $sth = $bot->{dbh}->prepare("
	select uc.gid, uc.url, uc.newurl
	from public.urlchecker uc
	join url url on url.id = uc.id
	where (
		lower(regexp_replace(regexp_replace(uc.url, '^https?://(www\.)?', 'http://'), '/\$', '')) = lower(regexp_replace(regexp_replace(uc.newurl, '^https?://(www\.)?', 'http://'), '/\$', ''))
		or uc.url ~ '(last.fm|facebook.com|www.emusic.com|guardian.co.uk|audionetwork.com|homepages.nyu.edu|7digital.com|hhgroups.com|avclub.com|daytrotter.com|cdjapan.co.jp|popmatters.com)/'
	)
	and uc.url != uc.newurl
    and uc.newurl !~ '^https://www.youtube.com/'
	and url.edits_pending = 0
	and uc.status = 301
	order by uc.status, uc.url
");
$sth->execute;

while (my ($gid, $url, $newurl) = $sth->fetchrow()) {
	unless ($max > 0) {
		$bot->print("Reached maximum number of files.\n");
		last;
	}

	my $tmpurlo = $url;
	$tmpurlo =~ s/^https?:\/\/(www\.)?/http:\/\//;
	$tmpurlo =~ s/\/$//;

	my $tmpurln = $newurl;
	$tmpurln =~ s/^https?:\/\/(www\.)?/http:\/\//;
	$tmpurln =~ s/\/$//;

	if ($url =~ /facebook\.com/) {
		$tmpurlo =~ s/\/(pages|people)\/[A-Za-z0-9%-]+\/([0-9]+)$/\/$1\/_\/$2/;
		$tmpurln =~ s/\/(pages|people)\/[A-Za-z0-9%-]+\/([0-9]+)$/\/$1\/_\/$2/;
		$tmpurlo =~ s/profile\.php\?id=/people\/_\//;

		$tmpurlo =~ s/\.([^.\/]+)$/$1/g;
		$tmpurln =~ s/\.([^.\/]+)$/$1/g;

		if ($tmpurlo =~ /^http:\/\/facebook.com\/(pages|people)\/_\/[0-9]+$/ && $tmpurln =~ /^http:\/\/facebook\.com\/[A-Za-z0-9.]+$/) {
			$tmpurln = $tmpurlo;
		}
	} elsif ($url =~ /last\.fm/) {
		$tmpurlo =~ s/%25/%/g;
		$tmpurlo =~ s/%20/+/g;
		$tmpurlo =~ s/%26/&/g;
		$tmpurlo =~ s/%2B/+/g;
		$tmpurlo =~ s/%3A/:/g;
		$tmpurlo =~ s/%2C/,/g;
		$tmpurlo =~ s/%E2%80%90/-/g;

		$tmpurln =~ s/(venue\/[0-9]+)\+[A-Za-z0-9'%.,()+-]+$/$1/;
	} elsif ($url =~ /www\.emusic\.com/) {
		$tmpurlo =~ s/\/(artist)\/[A-Za-z0-9%-]+\/([0-9]+)\.html/\/$1\/-\/$2/;
		$tmpurlo =~ s/\/(album)\/[A-Za-z0-9%-]+\/([0-9]+)\.html/\/$1\/-\/-\/$2/;
	} elsif ($url =~ /guardian\.co\.uk/) {
		$tmpurlo =~ s/guardian\.co\.uk\//theguardian\.com\//;
	} elsif ($url =~ /audionetwork\.com/) {
		$tmpurlo =~ s/\/cd\/ANW-[0-9]{4}-/\/album\//;
	} elsif ($url =~ /homepages\.nyu\.edu/) {
		$tmpurlo =~ s/homepages\.nyu\.edu\/~([^\/]+)\//files.nyu.edu\/$1\/public\//;
	} elsif ($url =~ /7digital\.com/) {
		$tmpurlo =~ s/artists\/([^\/]+)\/([^\/]+)$/artist\/$1\/release\/$2/;
	} elsif ($url =~ /hhgroups\.com/) {
		$tmpurln =~ s/\/albumes\/([^\/]+)\/([^\/]+)-([0-9]+)$/\/maqueta-$3\/$1-$2/;
	} elsif ($url =~ /avclub\.com/) {
		$tmpurlo =~ s/\/articles\/(.*),([0-9]+)$/\/review\/$2/;
		$tmpurln =~ s/\/(review|article)\/.*-([0-9]+)$/\/review\/$2/;
	} elsif ($url =~ /daytrotter\.com/) {
		$tmpurlo =~ s/\/dt\/([^\/]+)-concert\/([0-9-]+)\.html$/\/#!\/concert\/$1\/$2/;
	} elsif ($url =~ /cdjapan\.co\.jp/) {
		$tmpurlo =~ s/\/detailview\.html\?KEY=([A-Z]{4}-[0-9]+)$/\/product\/$1/;
	} elsif ($url =~ /popmatters\.com/) {
		$tmpurlo =~ s/\/pm\//\//;
#	} elsif ($url =~ //) {
#	} elsif ($url =~ //) {
#	} elsif ($url =~ //) {
#	} elsif ($url =~ //) {
#	} elsif ($url =~ //) {
#	} elsif ($url =~ //) {
#	} elsif ($url =~ //) {
#	} elsif ($url =~ //) {

	}

	$bot->print("Temporary old URL: $tmpurlo\nTemporary new URL: $tmpurln\n");

	next unless lc($tmpurlo) eq lc($tmpurln);

	$bot->print("Editing URL:\nOld: $url\nNew: $newurl\n\n");
	my $rv = $bot->edit_url($gid, { url => $newurl, as_auto_editor => 1, edit_note => "Following redirect" });
	$max -= $rv;
}


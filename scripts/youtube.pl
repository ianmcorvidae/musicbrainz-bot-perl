#!/usr/bin/perl
# perl bot.pl [options]

use FindBin;
use lib "$FindBin::Bin/..";
use utf8;

use MusicBrainzBot;

my $bot = MusicBrainzBot->new;
my $max = $bot->{settings}->{max};
$bot->login();

my $sth = $bot->{dbh}->prepare("
	select uc.gid, uc.url, uc.newurl
	from public.urlchecker uc
	join url url on url.id = uc.id
	where uc.url ~ '^https?://(www.)?youtube.com/'
	and url.edits_pending = 0
	and uc.status = 301
	order by uc.status, uc.url
");
$sth->execute;

while (my ($gid, $url, $newurl) = $sth->fetchrow()) {
	unless ($max > 0) {
		$bot->print("Reached maximum number of files.\n");
		last;
	}

	my $tmpurl = $url;
	$tmpurl =~ s/^https?:\/\/(?:www\.)?youtube\.com\/(?:user\/)?([A-Za-z0-9]+)\/?$/http:\/\/www.youtube.com\/user\/$1/;

	next unless lc($tmpurl) eq lc($newurl);

	$bot->print("Editing YouTube URL:\nOld: $url\nNew: $newurl\n\n");
	my $rv = $bot->edit_url($gid, { url => $newurl, as_auto_editor => 1, edit_note => "Following redirect" });
	$max -= $rv;
}


use utf8;

sub mapping {
	my %mapping = (
		# Germany - Berlin
		"Columbiahalle, Berlin, Germany" => "d07e5ba7-569c-4036-b5ae-984ef0fa9c27",
		"Kindl-Buehne Wuhlheide, Berlin, Germany" => "d65e0d44-0289-41f7-a867-2780c46ae51d",
		"The Sector Club, Berlin, Germany" => "7e090fa0-744c-4d5e-9ad9-fc725f171b2c", # 2
		"O2 World, Berlin, Germany" => "ef7f86c6-23a7-4f2c-92b7-517362682799", # 20
		"Berliner Philharmonie, Berlin, Germany" => "bea135c0-a32e-49be-85fd-9234c73fa0a8", # 1
		"Waldbühne, Berlin, Germany" => "80953ce4-96e9-440f-a952-c00b06885b7a", # 19

		# Germany - Düsseldorf
		"Philipshalle, Düsseldorf, Germany" => "c250a79e-da32-4c07-a93f-cbddaab51288",
		"Rheinstadion, Düsseldorf, Germany" => "6aea998a-e36d-4c79-9d99-ed1548d6c50e",
		"LTU Arena, Düsseldorf, Germany" => "b1203c45-ae31-4db9-afaf-d831d6a546e2",
		"Stahlwerk, Düsseldorf, Germany" => "0a242184-7ca7-4e9e-95ce-6b4291a41cb9", # 1

		# Germany - Köln
		"Lanxess Arena, Köln, Germany" => "9c67b435-e7ac-44fe-a009-a4db6c546f4b",
		"Kölner E-Werk, Cologne, Germany" => "f5ac7c33-f858-4328-9931-90073f747e3b",
		"E-Werk, Cologne, Germany" => "f5ac7c33-f858-4328-9931-90073f747e3b",
		"Palladium, Cologne, Germany" => "1ad553ee-dcf4-4c3a-977f-e36ffe97f56e",
		"Live Music Hall, Cologne, Germany" => "77cef78c-ab3e-427f-8362-c73d3cbc4e2e", # 14

		# Germany - Hamburg
		"Markthalle, Hamburg, Germany" => "310cb806-a349-460e-8a3d-d0a1c85014e1", # 1
		"Logo, Hamburg, Germany" => "64db6e72-5c11-4390-8ee1-39b83bf4d784", # 1
		"Große Freiheit 36, Hamburg, Germany" => "71ff68e6-4613-4c6d-9d8b-12494aca529c", # 20

		# Germany - Others
		"Rock am Ring, Nürburgring, Germany" => "7643f13a-dcda-4db4-8196-3ffcc1b99ab7",
		"Centralstation, Darmstadt, Germany" => "9dbef2fd-ff02-45d5-8225-786d53152448",
		"Turbinenhalle, Oberhausen, Germany" => "a1911142-49ce-4456-949b-5395b104e2d2",
		"Muffathalle, Munich, Germany" => "87d7f6b7-1592-4932-a333-6f4284f89c3b",
		"Circus Krone, Munich, Germany" => "d9b576de-cfbb-45a6-a6f3-6a7b0df89088", # 3
		"Liederhalle, Stuttgart, Germany" => "74cfad57-cd94-43c6-b0de-dcbcd3b1463a", # 7
		"Colos-Saal, Aschaffenburg, Germany" => "03d33773-2737-4d97-8591-6d1896cb9e84", # 1
		"Batschkapp, Frankfurt am Main, Germany" => "7f800a8e-1399-4df5-85f6-d5f57661ec44", # 2
		"Biskuithalle, Bonn, Germany" => "59f7e8d4-aa26-41fe-92ba-2efdaee1b346", # 1

		# Japan - Osaka
		"Muse Hall, Osaka, Japan" => "fed4574d-2756-442e-8588-93e7a313c87b",
		"OSAKA MUSE HALL, Japan" => "fed4574d-2756-442e-8588-93e7a313c87b",
		"Osaka-jō Hall, Osaka, Japan" => "50cdb1ed-2084-45b7-8f5d-e2e18c0d370c",
		"Osaka Castle Hall, Osaka, Japan" => "50cdb1ed-2084-45b7-8f5d-e2e18c0d370c",
		"Castle Hall, Osaka, Japan" => "50cdb1ed-2084-45b7-8f5d-e2e18c0d370c",
		"Festival Hall, Osaka, Japan" => "d3203594-47e6-47e2-8a75-8cfef53ffc0c",
		"Ōsaka Festival Hall, Japan" => "d3203594-47e6-47e2-8a75-8cfef53ffc0c",
		"NHK Hall, Osaka, Japan" => "69892d13-041d-4d1a-8187-9f9914197b29",
		"Shinsaibashi Club Quattro, Osaka, Japan" => "27c0378f-0cfa-4920-8870-ea9b9bc5828d",
		"Kosei Nenkin Kaikan, Osaka, Japan" => "751f998a-60ca-4d48-954f-b101d59ad89a",
		"Kousei Nenkin Kaikan, Osaka, Japan" => "751f998a-60ca-4d48-954f-b101d59ad89a",
		"Koseinenkin Hall, Osaka, Japan" => "751f998a-60ca-4d48-954f-b101d59ad89a",
		"Kouseinennkin Kaikan, Osaka, Japan" => "751f998a-60ca-4d48-954f-b101d59ad89a",
		"Kousei Nenkin Kaikan Hall, Osaka, Japan" => "751f998a-60ca-4d48-954f-b101d59ad89a",

		# Japan - Tokyo
		"Budokan, Tokyo, Japan" => "4d43b9d8-162d-4ac5-8068-dfb009722484",
		"Budokan Hall, Tokyo, Japan" => "4d43b9d8-162d-4ac5-8068-dfb009722484",
		"Nippon Budōkan, Tōkyō, Japan" => "4d43b9d8-162d-4ac5-8068-dfb009722484",
		"日本武道館, Tōkyō, Japan" => "4d43b9d8-162d-4ac5-8068-dfb009722484",
		"日本武道館, Tokyo, Japan" => "4d43b9d8-162d-4ac5-8068-dfb009722484",
		"Nippon Budokan Hall, Tokyo, Japan" => "4d43b9d8-162d-4ac5-8068-dfb009722484",
		"Nippon Budokan, Tokyo, Japan" => "4d43b9d8-162d-4ac5-8068-dfb009722484",
		"Nakano Sunplaza, Tokyo, Japan" => "44f57837-8773-4e69-b0e7-3360ddbe278a",
		"Nakano Sun Plaza, Tokyo, Japan" => "44f57837-8773-4e69-b0e7-3360ddbe278a",
		"Nakano SUNPLAZA, Japan" => "44f57837-8773-4e69-b0e7-3360ddbe278a",
		"Shibuya-AX, Tokyo, Japan" => "7254b738-4147-4298-a6e8-7587780392a7",
		"SHIBUYA-AX, Tōkyō, Japan" => "7254b738-4147-4298-a6e8-7587780392a7",
		"NHK Hall, Tokyo, Japan" => "fa8abb2c-4ca0-4838-b374-9c3c88921c77",
		"NHK HALL, Tōkyō, Japan" => "fa8abb2c-4ca0-4838-b374-9c3c88921c77",
		"Zepp Tokyo, Japan" => "bc1c475b-e265-4b0c-8283-4b3d1d87cf5f",
		"Zepp, Tokyo, Japan" => "bc1c475b-e265-4b0c-8283-4b3d1d87cf5f",
		"Zepp Tokyo, Tokyo, Japan" => "bc1c475b-e265-4b0c-8283-4b3d1d87cf5f",
		"Tokyo Koseinenkin Kaikan, Tokyo, Japan" => "c20359e1-0637-4957-8633-509496de89d9",
		"Tokyo Kōsei Nenkin Kaikan, Tokyo, Japan" => "c20359e1-0637-4957-8633-509496de89d9",
		"Tokyo Dome, Tokyo, Japan" => "9ebc6213-bd3d-40bb-b1d8-cc6d9f9a68fa",
		"Yoyogi National Gymnasium, Tokyo, Japan" => "38c64b31-c3b4-48f4-b840-d2cc2b9de6e7",
		"Shinjuku Koseinenkin Kaikan, Tokyo, Japan" => "c20359e1-0637-4957-8633-509496de89d9",
		"Shinjuku Kouseinenkin Kaikan Hall, Tokyo, Japan" => "c20359e1-0637-4957-8633-509496de89d9",
		"Kosei Nekin Kaiken Hall, Tokyo, Japan" => "c20359e1-0637-4957-8633-509496de89d9",
		"Hibiya Open-Air Concert Hall, Tokyo, Japan" => "afcfbe70-e168-435a-b0fe-3c556d5b5691",
		"Akasaka BLITZ, Tokyo, Japan" => "506f7fca-a300-4826-af0f-3d7787e0073e",
		"AKASAKA BLITZ, Tōkyō, Japan" => "506f7fca-a300-4826-af0f-3d7787e0073e",
		"Club Citta', Tokyo, Japan" => "501f4596-ef30-44d8-8501-9fd38727530a",
		"Club Citta, Tokyo, Japan" => "501f4596-ef30-44d8-8501-9fd38727530a",
		"Shibuya O-EAST, Tōkyō, Japan" => "194cd897-7894-4e5c-9f28-ea48373b20d9",
		"Shibuya O-WEST, Tōkyō, Japan" => "eac5ce60-3280-479e-9a38-66d6d2e4c9ff",
		"渋谷C.C.Lemonホール, Tōkyō, Japan" => "ce02592c-e1ad-4e3f-9382-07dd9e4490c6",
		"Shibuya Public Hall, Tokyo, Japan" => "ce02592c-e1ad-4e3f-9382-07dd9e4490c6",

		# Japan - Yokohama
		"Yokohama Arena, Yokohama, Japan" => "0ad0262f-5252-4e0d-9a60-e32b8badf659",
		"Yokohama Stadium, Japan" => "6ed7e3dc-1754-49cf-938f-aa4efcd31374",
		"YOKOHAMA BLITZ, Yokohama, Japan" => "48a895f2-1fa9-4ea3-a3dd-98f086fae4db",
		"Yokohama Blitz, Yokohama, Japan" => "48a895f2-1fa9-4ea3-a3dd-98f086fae4db",
		"Pacifico Yokohama, Yokohama, Japan" => "503b5953-3bab-4cdf-b2e4-470e1506440b",

		# Japan - Other
		"Hiroshima Sun Plaza Hall, Hiroshima, Japan" => "229ab714-eb87-43d8-b5eb-559122acaa08",
		"Club Quattro, Nagoya, Japan" => "f8d67bc7-0e5b-417b-9316-c46ca9854a20",
		"Izumity 21, Sendai, Japan" => "90086bad-8dac-4627-a6f4-c83a960b2ded",
		"Saitama Super Arena, Saitama, Japan" => "1e5ec994-f29c-455f-8be8-a1295a7135de",
		"さいたまスーパーアリーナ, Japan" => "1e5ec994-f29c-455f-8be8-a1295a7135de",
		"ZEPP, Fukuoka, Japan" => "dfe668a0-65ae-4e2f-a761-581fd49fe993",
		"the Zepp, Fukuoka, Japan" => "dfe668a0-65ae-4e2f-a761-581fd49fe993",

		# Philippines
		"Ninoy Aquino Stadium, Manila, Philippines" => "88d305b2-9d00-40cd-b120-9b785aea84c8", # 6

		# Thailand
		"Bangkok Hall, Bangkok, Thailand" => "de5e3396-4194-4664-8456-e0a1a5cca07a", # 2

		# Israel
		"City Hall, Haifa, Israel" => "3fba6a2f-e9ea-47dc-a406-d4c986c2e00e", # 5

		# Argentina
		"Teatro 25 de Mayo, Buenos Aires, Argentina" => "eec7686b-c0dd-401d-bba1-f1269765daed", # 21
		"Parque Sarmiento, Buenos Aires, Argentina" => "6cb24742-3431-4ad6-82e5-0de07246e1f1", # 21
		"Luna Park, Buenos Aires, Argentina" => "0033f0ce-c360-4ff2-a281-0a8b279d0511", # 2
		"Hangar, Buenos Aires, Argentina" => "772406bd-9ed5-466d-92ba-e9e067b6c806", # 15

		# South Africa
		"Green Point Stadium, Cape Town, South Africa" => "0a61c5bf-b574-4cd5-96c0-73fa57d507a0", # 17

		# Australia
		"Sydney Opera House, Sydney, Australia" => "a24c9284-a9d2-428b-bacd-fa79cf9a9108", # 1
		"Rod Laver Arena, Melbourne, Australia" => "78c130da-943c-433b-abfb-6af4c50314e1", # 102

		# Belgium
		"Ancienne Belgique, Brussels, Belgium" => "36ba61e0-6c79-415b-8515-aaa3bd55fa34", # 40

		# Switzerland
		"Z7, Pratteln, Switzerland" => "2bada9ac-d579-4c7b-86d2-b028bdd665da", # 3
		"Victoria Hall, Geneva, Switzerland" => "68f2f6b2-380d-4b57-af6e-2dc3e028c80d", # 4

		# Slovenia
		"Hala Tivoli, Ljubljana, Slovenia" => "a258727d-9c0f-4e54-90cd-32d5ab13107e", # 30

		# Turkey
		"Ali Sami Yen Stadium, Istanbul, Turkey" => "8fcc7987-2221-431f-9c00-500781f4c0bd", # 20

		# Italy
		"Parco Nord, Bologna, Italy" => "4f8f5ace-6562-4713-adc1-68c7198290d8", # 20

		# Spain
		"Sala Heineken, Madrid, Spain" => "b7a4ac08-7b20-41a4-b06c-12c47fcc2381", # 1
		"Kafe Antzokia, Bilbao, Spain" => "58a3b158-221c-43ff-ba3d-50cad93f564b", # 6
		"Drive Division Estudios, Santander, Spain" => "f8f94a86-ca24-48b0-a685-13cfc699430d", # 3
		"Aqualung, Madrid, Spain" => "be61687d-e9bb-4746-a21e-72a916886494", # 10

		# Sweden
		"Kulturhuset Spira, Jönköping, Sweden" => "22b9e9b6-68b3-4d66-a167-6b1a43932f31", # 1
		"Kulturens Hus, Luleå, Sweden" => "e7650489-e73d-4da9-9ca7-f3a6a6841970", # 12
		"Konserthuset, Stockholm, Sweden" => "270160cd-676e-474c-8894-caab3640d0b4", # 3
		"Kamraspalatset, Stockholm, Sweden" => "b9bbf687-41b4-477b-8abe-79064e48497f", # 11

		# Netherlands
		"Heineken Music Hall, Amsterdam, Netherlands" => "9d9210d3-e799-4b66-8616-c7b862d89c94", # 71
		"Concertgebouw, Amsterdam, Netherlands" => "8a6161bb-fb50-4234-82c5-1e24ab342499", # 31
		"Boerderij, Zoetermeer, Netherlands" => "b9020e9d-6483-437d-827c-0657fca58368", # 13
		"Ahoy Rotterdam, Rotterdam, Netherlands" => "d3b27cc1-d397-4357-bb42-f452f8e950c0", # 30
		"013, Tilburg, Netherlands" => "d54ee905-5072-42b9-a6c2-eecae0d7ce16", # 40

		# Norway
		"Sentrum Scene, Oslo, Norway" => "7b28a909-e7a5-4e3c-bfe3-04d20310af46", # 11
		"Rockefeller Music Hall, Oslo, Norway" => "5c78c79e-7fd6-47c3-9f4f-8d43fe9f5d4c", # 3

		# Poland
		"Śląski Stadium, Chorzów, Poland" => "e3c9967b-fcc8-48e9-b56c-342b0befe107", # 60

		# Finland
		"YO-talo, Tampere, Finland" => "6514cf0d-fe1b-4f32-bd6d-9046c535c399", # 20
		"Verkatehdas, Hämeenlinna, Finland" => "f9587914-8505-4bd1-833b-16a3100a4948", # 16
		"Tavastia Club, Helsinki, Finland" => "5d4bfd36-d7b8-4339-b4d0-1a031ba30b1b", # 36
		"Savoy-teatteri, Helsinki, Finland" => "5f3f09e4-90f5-4082-a375-f68eff3dbce4", # 1
		"Satulinna, Hattula, Finland" => "081bab93-75a1-4f20-bd07-032a6eedd763", # 15
		"Nosturi, Helsinki, Finland" => "ae79517f-4bba-4e01-9cc8-cf27e92b5f25", # 27
		"Musiikkitalo, Helsinki, Finland" => "0b373c0c-8195-4124-b090-79b48ec13d13", # 17
		"Hartwall Areena, Helsinki, Finland" => "a99742bf-1b3a-4907-98fb-bbed8b64a115", # 37
		"Fruugo, Helsinki, Finland" => "e249d3f0-4282-4eaf-8b6d-4cc7cd474950", # 1

		# Estonia
		"Õpetajate Maja, Tallinn, Estonia" => "1b850d4b-9a5c-4ca9-8fbc-d912b3751dfe", # 19
		"Von Krahl, Tallinn, Estonia" => "0ad8255e-5586-4c7d-accf-c11d203445bc", # 18
		"Vene Kultuurikeskus, Tallinn, Estonia" => "dc5edda7-e0b3-4dc0-aba6-48c3f98dcbef", # 6
		"Vanemuise kontserdimaja, Tartu, Estonia" => "5228f14a-ddc6-4c1e-922a-520a4c2933c3", # 11
		"Ungern-Sternbergide palee, Tallinn, Estonia" => "7a35c43b-c31b-45de-9b0c-94e0bbeb7472", # 17
		"Tartu Ülikooli aula, Tartu, Estonia" => "b40a4f68-5d3f-4b0b-a1fa-bae563079ba2", # 23
		"Tartu Ülikooli ajaloo muuseum, Tartu, Estonia" => "9bc068c2-1304-4d19-bb33-2b6240ff27d7", # 1
		"Tartu Linnamuuseum, Tartu, Estonia" => "fe552d65-c0ee-4bad-8108-010eb07bfac2", # 1
		"Tallinna raekoda, Tallinn, Estonia" => "409e7ba1-b8a7-4e6e-803d-a63420252183", # 13
		"Tallinna Keskraamatukogu, Tallinn, Estonia" => "4b9acb27-3da4-4479-82e9-3e6f944894fb", # 3
		"Sillapää loss, Räpina, Estonia" => "31c5d931-b064-410e-921b-567a3a08d717", # 1
		"Salemi kirik, Tartu, Estonia" => "ab58d43e-8fac-4913-9fb9-d02e8883f0a6", # 16
		"Rootsi-Mihkli kirik, Tallinn, Estonia" => "4d7d6f57-8822-404a-8352-5895c932bb9a", # 28
		"Rock Cafe, Tallinn, Estonia" => "bd741a9e-2b3a-4565-9adb-8d6afc5916bd", # 30
		"Raekoja saal, Tartu, Estonia" => "4264a322-aab2-4a01-8972-77432b6c44a9", # 2
		"Plink Plonk, Tartu, Estonia" => "934578eb-5d83-499e-be42-917b7d1c6a35", # 9
		"Okupatsioonide muuseum, Tallinn, Estonia" => "65529d38-bd61-4237-944e-d236033bfbd1", # 1
		"Mustpeade maja, Tallinn, Estonia" => "ac589f0f-f03a-48ae-9037-3e559fecec18", # 9
		"Metodisti kirik, Tallinn, Estonia" => "e4f00a61-78af-4cd0-9c7c-d26f8bd45acc", # 11
		"Matkamaja, Tallinn, Estonia" => "e53e4794-d10f-445f-84af-895fa3187c45", # 2
		"Kadrioru loss, Tallinn, Estonia" => "08ef13f8-f690-4cd1-a5b4-974e128866de", # 22
		"Jaani kirik, Tartu, Estonia" => "86b04ace-ca69-4142-bf95-31b035f3bea4", # 1
		"Jaani kirik, Tallinn, Estonia" => "4390b0a6-2a97-4a1e-ab4e-90a05b0064b4", # 14
		"Estonia kontserdisaal, Tallinn, Estonia" => "fd9b9ea3-1c61-4d27-a666-50fe77356b2e", # 166
		"Eliisabeti kirik, Pärnu, Estonia" => "1089da0c-4e79-4442-ad22-2477b5d28ba7", # 6
		"EMTA orelisaal, Tallinn, Estonia" => "4802e9c8-4269-47ee-bc00-4e7471fd4191", # 11
		"EMTA ooperistuudio, Tallinn, Estonia" => "ba590de6-c163-4610-bb5d-97206c116b8c", # 2
		"EMTA kammersaal, Tallinn, Estonia" => "1face1f5-03e4-44df-978c-beee23fb3937", # 130
		"Club Privé, Tallinn, Estonia" => "c9413e7b-e3b7-46f3-ab87-b09cfd8077f0", # 26

	);

	return %mapping;
}

1;


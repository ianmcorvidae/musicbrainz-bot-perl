Dependencies
------------

* DBI
* Getopt::Long
* JSON
* List::MoreUtils
* WWW::Mechanize


Settings
--------

Settings are stored as JSON in settings/. The default filename is default.json. A sample file can be found in settings/default.json.sample.

TODO:
dbname
dbuser

add useragent


Usage
-----

The default command line parameters understood by the module are:

--username -u
  The username to use when logging into the website.

--password -p
  The password to use when logging into the website.

--server
  The hostname to use for loading the website.
  default: musicbrainz.org

--protocol
  The protocol to use for loading the website.
  default: https

--max -m
  The maximum number of edits to make.
  default: 1000

--verbose -v
  Print strings such as debugging information which would not normally be shown.

--dryrun
  Do not try to login or make any edits.


Writing scripts using the bot module
------------------------------------

Load the bot module:
 use MusicBrainzBot;

Create a bot object with the default settings file:
 my $bot = MusicBrainzBot->new;

Alternatively, create a bot object with a custom settings file:
 my $bot = MusicBrainzBot->new({ settings => "some_other_settings.json" });

The database handler is stored in $bot->{dbh}. You can prepare a SQL query using:
 my $sth = $bot->{dbh}->prepare("select gid, name from artist limit 5");
This will give you a normal DBI statement handler to work with.

The bot will automatically try to log in when necessary and prompt for a username and/or password if none has been provided already.
If you want to trigger the login at a certain point for manually run scripts, for example before running a query which takes a long time, you can do that using:
 $bot->login();

If you want to print string which are only shown when verbose is turned on, as is often the case for bot scripts, use:
 $bot->print("My extra information here");

For adding entities, the bot has the following methods:
 * add_artist
 * add_label
 * add_release_group
 * add_recording
 * add_work
 * add_place
 * add_area
 * add_series
 * add_instrument
 * add_event
They take a hash of values to set as a parameter and return an MBID if successful.
Adding releases is not currently supported.

Example:
 my $mbid = $bot->add_artist({ name => "Name", sort_name => "Sortname" });

For editing entities,
 * edit_artist
 * edit_label
 * edit_release_group
 * edit_recording
 * edit_work
 * edit_place
 * edit_area
 * edit_series
 * edit_instrument
 * edit_event
They take an MBID and a hash of values to set as parameters.
Editing releases is not currently supported.

Example:
  my $rv = $bot->edit_artist($mbid, { name => "New Name" });

Removing entities is currently not supported.

For adding, editing or removing relationships, there is currently $bot->edit_relationships_new() which takes a hash of values
in the format described on https://wiki.musicbrainz.org/User:Bitmap/Relationship_Editor_Parameters
This method is experimental and will almost certainly change in the future.


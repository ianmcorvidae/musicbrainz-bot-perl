#!/usr/bin/perl

package MusicBrainzArea;
use strict;
use utf8;
use warnings;

use DBI;
use JSON;
use LWP::Simple;

sub new {
	my ($package, $dbh, $mbid) = @_;
	my $self = {
		'dbh' => $dbh,
		'mbid' => $mbid,
		'wikidata' => "",
		'aliases' => [],
	};
	my $obj = bless $self, $package;

	$obj->_load_wikidata;
	$obj->_load_aliases;
	$obj->_load_wikidata_data;

	return $obj;
}

sub _load_wikidata {
	my $self = shift;

	my $sth = $self->{dbh}->prepare("select regexp_replace(url, '.*/', '') as qid from area join l_area_url on entity0=area.id join url on entity1=url.id where url ~ 'wikidata' and area.gid = ?");
	$sth->execute($self->{mbid});

	my @ids = ();
	while (my ($qid) = $sth->fetchrow()) {
		push @ids, $qid;
	}

	if (scalar @ids > 1) {
		print "$self->{mbid} has more than one Wikidata ID\n";
	} else {
		$self->{wikidata} = shift @ids;
	}
}

sub _load_aliases {
	my $self = shift;

	my $sth = $self->{dbh}->prepare("select aa.id, aa.name, aa.sort_name, aa.locale, aa.primary_for_locale from area a join area_alias aa on aa.area = a.id where a.gid = ?");
	$sth->execute($self->{mbid});

	while (my ($id, $name, $sortname, $locale, $primary) = $sth->fetchrow()) {
		push @{ $self->{aliases} }, { id => $id, name => $name, sortname => $sortname, locale => $locale, primary => $primary };
		$self->{aliases2}->{ $locale } = { id => $id, name => $name, sortname => $sortname, locale => $locale, primary => $primary };
	}
}

sub _load_wikidata_data {
	my $self = shift;

	my $data = get_page($self->{wikidata});
	my $q = $data->{entities}->{ $self->{wikidata} };

	for my $l (sort keys %{ $q->{labels} }) {
		push @{ $self->{wikidata_aliases} }, { locale => $l, name => $q->{labels}->{$l}->{value} };
		$self->{wikidata_aliases2}->{ $l } = { locale => $l, name => $q->{labels}->{$l}->{value} };
	}

	my @countries = ();
	for my $c (@{ $q->{claims}->{"P17"} }) {
		next if $c->{qualifiers}->{P582};
		push @countries, $c->{mainsnak}->{datavalue}->{value}->{"numeric-id"};
	}

	if (scalar @countries > 1) {
		print "$self->{mbid} has more than one country in Wikidata\n";
	} else {
		$self->{country} = shift @countries;
	}
}

sub get_page {
	my ($id) = shift;

	my $pagedataj = get("http://www.wikidata.org/w/api.php?action=wbgetentities&ids=$id&format=json");
	my $pagedata = decode_json($pagedataj);
	sleep 1;

	return $pagedata;
}

sub country {
	my $self = shift;
	return $self->{country};
}

sub compare {
	my $self = shift;
	my $locale = shift;
	my $language = lc($locale);
	$language =~ s/_/-/g;

	if ($self->{aliases2}->{$locale}) {
		# We have an alias for this language already, compare them
		printf ("%s does not match %s and may need updating\n", $self->{aliases2}->{$locale}->{name}, $self->{wikidata_aliases2}->{$language}->{name}) if $self->{aliases2}->{$locale}->{name} ne $self->{wikidata_aliases2}->{$language}->{name};
	} elsif ($self->{wikidata_aliases2}->{$language}) {
		# No existing alias, add one
#		printf "Add %s\t%s\n", "$language", $self->{wikidata_aliases2}->{$language}->{name};
		return $self->{wikidata_aliases2}->{$language}->{name};
	}

}

sub print_details {
	my $self = shift;

	print "MBID: ", $self->{mbid}, "\n";
	print "Wikidata ID: ", $self->{wikidata}, "\n";
	print "Country: ", $self->{country}, "\n";

	if ($self->{aliases} && scalar @{ $self->{aliases} }) {
		print "Aliases:\n";

		for my $a (@{ $self->{aliases} }) {
			printf "%s\t%s\t%s\t%s\t%s\n",
				$a->{id},
				$a->{name},
				$a->{sortname},
				$a->{locale},
				$a->{primary},
			;
		}
	} else {
		print "No aliases found.\n";
	}

#	for my $a (@{ $self->{wikidata_aliases} }) {
#		printf "%s\t%s\n",
#			$a->{locale},
#			$a->{name},
#		;
#	}

	print "\n";
}

1;


#!/usr/bin/perl

package MusicBrainzBot::DB;
use strict;
use utf8;
use warnings;

use DBI;

sub new {
	my ($package, $settings) = @_;
	my $self = { settings => $settings };
	my $obj = bless $self, $package;

	$obj->_db_setup;

	return $obj;
}

sub _db_setup {
	my $self = shift;

	my $dbname = $self->{settings}->get("dbname");
	my $dbuser = $self->{settings}->get("dbuser");

	$self->{dbh} = DBI->connect("dbi:Pg:dbname=$dbname", $dbuser, '', { pg_enable_utf8 => 1 }) or die;
	$self->{dbh}->do("SET search_path TO musicbrainz");
}

sub prepare {
	my $self = shift;
	my $query = shift;

	return $self->{dbh}->prepare($query);
}

1;


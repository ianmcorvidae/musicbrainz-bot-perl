#!/usr/bin/perl

package MusicBrainzBot::Settings;
use strict;
use utf8;
use warnings;

use JSON;
use List::MoreUtils qw(uniq);
use Getopt::Long;

sub new {
	my ($package, $args) = @_;
	my $file = $args->{"settings"} || "default.json";

	# FIXME: Support absolute paths
	open SETTINGS, "$FindBin::Bin/../settings/$file" or die "Could not open settings file $FindBin::Bin/../settings/$file.\n";
	my $settingsj = join "", <SETTINGS>;
	close SETTINGS;
	my $self = decode_json($settingsj);

	my @options = ('username|u=s', 'password|p=s', 'server=s', 'protocol=s', 'verbose|v', 'max|m=i', 'dryrun');
	GetOptions(\%$self, @options);

	$self->{max} = 1000 unless $self->{max};

	$self->{wikidata_country_locale_mapping} = {
#		15180 => [ qw() ], # Soviet Union

# Europe
		222 => [ qw(sq) ], # Albania
		228 => [ qw(ca) ], # Andorra
		40 => [ qw(de) ], # Austria
		184 => [ qw(be ru) ], # Belarus + en
		31 => [ qw(de fr nl) ], # Belgium
		225 => [ qw(bs sr) ], # Bosnia and Herzegovina + sh sr-cyrl sr-latn
		219 => [ qw(bg) ], # Bulgaria
		224 => [ qw(hr) ], # Croatia
		229 => [ qw(el tr) ], # Cyprus
		213 => [ qw(cs) ], # Czech Republic
#		33946 => [ qw() ], # Czechoslovakia
		35 => [ qw(da) ], # Denmark
#		16957 => [ qw() ], # East Germany
		191 => [ qw(et ru) ], # Estonia
		4628 => [ qw(fo) ], # Faroe Islands
#		1410 => [ qw() ], # Gibraltar
#		25230 => [ qw() ], # Guernsey
		33 => [ qw(fi sv se) ], # Finland + se
		142 => [ qw(fr) ], # France + oc
		183 => [ qw(de) ], # Germany + nds
		41 => [ qw(el) ], # Greece
		28 => [ qw(hu) ], # Hungary
		189 => [ qw(is) ], # Iceland
		27 => [ qw(ga) ], # Ireland + en
#		9676 => [ qw() ], # Isle of Man
		38 => [ qw(it) ], # Italy + fur
#		785 => [ qw() ], # Jersey
		211 => [ qw(lv ru) ], # Latvia
		347 => [ qw(de) ], # Liechtenstein
		37 => [ qw(lt ru) ], # Lithuania
		32 => [ qw(de fr) ], # Luxembourg
		221 => [ qw(mk) ], # Macedonia
		233 => [ qw(mt) ], # Malta + en
		217 => [ qw(ro ru) ], # Moldova
		235 => [ qw(fr) ], # Monaco
		236 => [ qw(sr) ], # Montenegro + sr-cyrl sr-latn
		55 => [ qw(nl) ], # Netherlands
		20 => [ qw(nb nn) ], # Norway + no se
		36 => [ qw(pl) ], # Poland
		45 => [ qw(pt) ], # Portugal
		218 => [ qw(ro) ], # Romania
		159 => [ qw(ru) ], # Russian Federation + tt
		238 => [ qw(it) ], # San Marino
		403 => [ qw(sr) ], # Serbia + sh sr-cyrl sr-latn
		37024 => [ qw(sr) ], # Serbia and Montenegro + sh sr-cyrl sr-latn
		214 => [ qw(sk) ], # Slovakia
		215 => [ qw(si) ], # Slovenia
		29 => [ qw(es) ], # Spain + ca eu gl
		842829 => [ qw(nb nn) ], # Svalbard and Jan Mayen
		34 => [ qw(sv) ], # Sweden
		39 => [ qw(de fr it) ], # Switzerland + gsw
		43 => [ qw(tr ku) ], # Turkey + ku-latn
		212 => [ qw(ru uk) ], # Ukraine
		145 => [ qw(cy gv kw) ], # United Kingdom + en
#		237 => [ qw() ], # Vatican City State (Holy See)
		5689 => [ qw(fi sv) ], # Åland Islands

# Caucasus & Central Asia
		399 => [ qw(hy ru) ], # Armenia
		227 => [ qw(az ru) ], # Azerbaijan + az-cyrl az-latn
		230 => [ qw(ka ru) ], # Georgia
		232 => [ qw(kk ru) ], # Kazakhstan + kk-cyrl
		813 => [ qw(ky ru) ], # Kyrgyzstan
		863 => [ qw(tg ru) ], # Tajikistan + tg-cyrl
#		874 => [ qw() ], # Turkmenistan
		265 => [ qw(uz ru) ], # Uzbekistan + uz-cyrl uz-latn

# East Asia
		148 => [ qw(zh_Hans zh_Hant) ], # China + bo ii mn mn-mong ug ug-arab
		8646 => [ qw(zh_Hans zh_Hant) ], # Hong Kong
		17 => [ qw(ja) ], # Japan
		14773 => [ qw(zh_Hans zh_Hant) ], # Macao
		711 => [ qw(mn) ], # Mongolia + mn-cyrl
		423 => [ qw(ko) ], # North Korea
		884 => [ qw(ko) ], # South Korea
		865 => [ qw(trv zh_Hans zh_Hant) ], # Taiwan

# South-East Asia
		921 => [ qw(ms) ], # Brunei
		424 => [ qw(km) ], # Cambodia
		252 => [ qw(id) ], # Indonesia
		819 => [ qw(lo) ], # Laos
		833 => [ qw(ms) ], # Malaysia
		836 => [ qw(my) ], # Myanmar
		928 => [ qw(fil) ], # Philippines + en
		334 => [ qw(zh_Hans zh_Hant) ], # Singapore + en
		869 => [ qw(th) ], # Thailand
#		574 => [ qw() ], # Timor-Leste
		881 => [ qw(vi) ], # Vietnam

# South Asia
		889 => [ qw(fa ps uz) ], # Afghanistan + uz-arab
		902 => [ qw(bn) ], # Bangladesh
		917 => [ qw(dz) ], # Bhutan
		668 => [ qw(as bn bo gu hi kn kok ml mr ne or pa sa ta te ur) ], # India + en pa-guru
		794 => [ qw(fa ku) ], # Iran + ku-arab
		837 => [ qw(ne) ], # Nepal
		843 => [ qw(pa ur) ], # Pakistan + en pa-arab
		854 => [ qw(si) ], # Sri Lanka


# Middle East
		398 => [ qw(ar) ], # Bahrain
		796 => [ qw(ar ku) ], # Iraq + ku-arab
		801 => [ qw(he) ], # Israel
		810 => [ qw(ar) ], # Jordan
		817 => [ qw(ar) ], # Kuwait
		822 => [ qw(ar) ], # Lebanon
		219060 => [ qw(ar) ], # Palestine
		842 => [ qw(ar) ], # Oman
		846 => [ qw(ar) ], # Qatar
		851 => [ qw(ar) ], # Saudi Arabia
		858 => [ qw(ar ku syr) ], # Syria + ku-arab
		878 => [ qw(ar) ], # United Arab Emirates
		805 => [ qw(ar) ], # Yemen

# Oceania
#		408 => [ qw() ], # Australia + en
#		664 => [ qw() ], # New Zealand + en
#		16641 => [ qw() ], # American Samoa + en
#		26988 => [ qw() ], # Cook Islands
#		712 => [ qw() ], # Fiji
#		30971 => [ qw() ], # French Polynesia
#		16635 => [ qw() ], # Guam + en
#		710 => [ qw() ], # Kiribati
#		709 => [ qw() ], # Marshall Islands + en
#		702 => [ qw() ], # Micronesia, Federated States of
#		697 => [ qw() ], # Nauru
#		33788 => [ qw() ], # New Caledonia
#		34020 => [ qw() ], # Niue
#		31057 => [ qw() ], # Norfolk Island
#		16644 => [ qw() ], # Northern Mariana Islands + en
#		695 => [ qw() ], # Palau
#		691 => [ qw() ], # Papua New Guinea
#		35672 => [ qw() ], # Pitcairn
#		683 => [ qw() ], # Samoa
#		685 => [ qw() ], # Solomon Islands
#		36823 => [ qw() ], # Tokelau
		678 => [ qw(to) ], # Tonga
#		672 => [ qw() ], # Tuvalu
#		16645 => [ qw() ], # United States Minor Outlying Islands + en
#		686 => [ qw() ], # Vanuatu
#		35555 => [ qw() ], # Wallis and Futuna

# Indian Ocean
#		43448 => [ qw() ], # British Indian Ocean Territory
#		31063 => [ qw() ], # Christmas Island
#		36004 => [ qw() ], # Cocos (Keeling) Islands
#		970 => [ qw() ], # Comoros
		826 => [ qw(dv) ], # Maldives
#		17063 => [ qw() ], # Mayotte
#		1027 => [ qw() ], # Mauritius
#		17070 => [ qw() ], # Réunion
#		1042 => [ qw() ], # Seychelles

# South Atlantic Ocean, Southern Ocean & Antarctica
#		1555938 => [ qw() ], # Antarctica
#		23408 => [ qw() ], # Bouvet Island
#		9648 => [ qw() ], # Falkland Islands
#		129003 => [ qw() ], # French Southern Territories
#		131198 => [ qw() ], # Heard Island and McDonald Islands
#		192184 => [ qw() ], # Saint Helena, Ascension and Tristan da Cunha
#		35086 => [ qw() ], # South Georgia and the South Sandwich Islands

# North America
#		23635 => [ qw() ], # Bermuda
		16 => [ qw(fr) ], # Canada + en
		223 => [ qw(da kl) ], # Greenland
		96 => [ qw(es) ], # Mexico
#		34617 => [ qw() ], # Saint Pierre and Miquelon
#		30 => [ qw(es haw) ], # United States + en en-dsrt

# Caribbean
#		25228 => [ qw() ], # Anguilla
#		781 => [ qw() ], # Antigua and Barbuda
#		21203 => [ qw() ], # Aruba
#		778 => [ qw() ], # Bahamas
#		244 => [ qw() ], # Barbados
#		27561 => [ qw() ], # Bonaire, Sint Eustatius and Saba
#		25305 => [ qw() ], # British Virgin Islands
#		5785 => [ qw() ], # Cayman Islands
#		241 => [ qw() ], # Cuba
#		25279 => [ qw() ], # Curaçao
#		784 => [ qw() ], # Dominica
		786 => [ qw(es) ], # Dominican Republic
#		769 => [ qw() ], # Grenada
#		17012 => [ qw() ], # Guadeloupe
#		790 => [ qw() ], # Haiti
#		766 => [ qw() ], # Jamaica + en
#		17054 => [ qw() ], # Martinique
#		13353 => [ qw() ], # Montserrat
#		25227 => [ qw() ], # Netherlands Antilles
		1183 => [ qw(es) ], # Puerto Rico
#		25362 => [ qw() ], # Saint Barthélemy
#		763 => [ qw() ], # Saint Kitts and Nevis
#		760 => [ qw() ], # Saint Lucia
#		126125 => [ qw() ], # Saint Martin (French part)
#		757 => [ qw() ], # Saint Vincent and The Grenadines
#		26273 => [ qw() ], # Sint Maarten (Dutch part)
#		754 => [ qw() ], # Trinidad and Tobago + en
#		18221 => [ qw() ], # Turks and Caicos Islands
#		11703 => [ qw() ], # U.S. Virgin Islands + en

# Central & South America
		414 => [ qw(es) ], # Argentina
#		242 => [ qw() ], # Belize + en
		750 => [ qw(es) ], # Bolivia
		155 => [ qw(pt) ], # Brazil
		298 => [ qw(es) ], # Chile
		739 => [ qw(es) ], # Colombia
		800 => [ qw(es) ], # Costa Rica
		736 => [ qw(es) ], # Ecuador
		792 => [ qw(es) ], # El Salvador
		774 => [ qw(es) ], # Guatemala
#		3769 => [ qw() ], # French Guiana
#		734 => [ qw() ], # Guyana
		783 => [ qw(es) ], # Honduras
		811 => [ qw(es) ], # Nicaragua
#		804 => [ qw() ], # Panama
		733 => [ qw(es) ], # Paraguay
		419 => [ qw(es) ], # Peru
#		730 => [ qw() ], # Suriname
		77 => [ qw(es) ], # Uruguay
		717 => [ qw(es) ], # Venezuela

# Africa
		262 => [ qw(ar) ], # Algeria
#		916 => [ qw() ], # Angola
#		962 => [ qw() ], # Benin
#		963 => [ qw() ], # Botswana + en
#		965 => [ qw() ], # Burkina Faso
#		967 => [ qw() ], # Burundi
#		1009 => [ qw() ], # Cameroon
#		1011 => [ qw() ], # Cape Verde
#		929 => [ qw() ], # Central African Republic
#		657 => [ qw() ], # Chad
		971 => [ qw(ln) ], # Congo
		1008 => [ qw(kfo) ], # Côte d'Ivoire
		974 => [ qw(ln) ], # Democratic Republic of the Congo
		977 => [ qw(aa so) ], # Djibouti
		79 => [ qw(ar) ], # Egypt
#		983 => [ qw() ], # Equatorial Guinea
		986 => [ qw(aa byn gez ti tig) ], # Eritrea
		115 => [ qw(aa am gez om sid so ti wal) ], # Ethiopia
#		1000 => [ qw() ], # Gabon
#		1005 => [ qw() ], # Gambia
		117 => [ qw(ak ee gaa ha) ], # Ghana + ha-latn
		1006 => [ qw(kpe) ], # Guinea
#		1007 => [ qw() ], # Guinea-Bissau
		114 => [ qw(kam om so sw) ], # Kenya
		1013 => [ qw(st) ], # Lesotho
		1014 => [ qw(kpe) ], # Liberia
		1016 => [ qw(ar) ], # Libya
#		1019 => [ qw() ], # Madagascar
		1020 => [ qw(ny) ], # Malawi
#		912 => [ qw() ], # Mali
#		1025 => [ qw() ], # Mauritania
		1028 => [ qw(ar) ], # Morocco
#		1029 => [ qw() ], # Mozambique
		1030 => [ qw(af) ], # Namibia + en
		1032 => [ qw(ha) ], # Niger + ha-latn
		1033 => [ qw(cch ha ig kaj kcg yo) ], # Nigeria + ha-arab ha-latn
		1037 => [ qw(rw) ], # Rwanda
#		1039 => [ qw() ], # Sao Tome and Principe
		1041 => [ qw(fr wo) ], # Senegal + wo-latn
#		1044 => [ qw() ], # Sierra Leone
		1045 => [ qw(so) ], # Somalia
		258 => [ qw(af nr nso ss st tn ts ve xh zu) ], # South Africa + en
#		958 => [ qw() ], # South Sudan
		1049 => [ qw(ar ha) ], # Sudan + ha-arab
		1050 => [ qw(ss) ], # Swaziland
		924 => [ qw(sw) ], # Tanzania
		945 => [ qw(ee) ], # Togo
		948 => [ qw(ar) ], # Tunisia
#		1036 => [ qw() ], # Uganda
#		6250 => [ qw() ], # Western Sahara
#		953 => [ qw() ], # Zambia
#		954 => [ qw() ], # Zimbabwe + en

	};

	bless $self, $package;
}

sub get {
	my $self = shift;
	my $var = shift;

	return $self->{$var};
}

sub get_languages {
	my $self = shift;
	my $var = shift;

#	my $languages = $self->{wikidata_country_locale_mapping}->{$var};
#	if (!$languages) { return $languages; }

	my $languages = $self->{wikidata_country_locale_mapping}->{$var} || [];
	push @$languages, ("en", "de", "ja"); # FIXME: This ends up repeatedly adding "en", which is why uniq is needed below

	return uniq @$languages;
}

1;

